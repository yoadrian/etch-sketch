# Etch-a-Sketch

Draw your masterpiece here [https://yoadrian.gitlab.io/etch-sketch/](https://yoadrian.gitlab.io/etch-sketch)

<img loading="lazy" src="./adrian-dobre-codeberg-etch-sketch.png" alt="etch-a-sketch game screen grab" />

## Project Objectives

Game Objectives (taken from the project's [page](https://www.theodinproject.com/lessons/foundations-etch-a-sketch)
- Create a webpage with a 16x16 grid of square divs, the divs should be implemented through JavaScript
- Set up a "hover" effect so that the grid divs change color when your mouse passes over them, leaving a (pixelated) trail through your grid like a pen would
- Posibility to resize the grid
- Draw with a random color and add 10% of black to it so that only after 10 passes is the square completely black

Meta Objectives
- limit the amount of variables defined in global scope to as few as possible
- keep functions to a maximum of two-level indentations as much as possible
- design objective: make the similar to the actual physical device, not square if possible
- implement a function to turn the brush On/Off
- implement a function to shake the game when resetting the board

## How I Approached Building the Project

I planned the project using these steps
- Information Gathering
    - I started by researching and creating a document of reseources for the entire project, and updated it on the go
- Planning
    - the plan for building the app was to deliver a working piece of code for each feature it has, in a certain timeframe
- Design
    - design objective: make the similar to the actual physical device
    - used Penpot to make a wireframe of how I envisioned the end product
    - The predominant color is Viva Magenta, Pantone's Color of the Year 2023
- Code
    - I buillt the app with minimal HTML and CSS, giving priority to the JS logic
    - limited the amount of variables defined in global scope to as few as possible
    - kept functions to a maximum of two-level indentations as much as possible
- Test
    - I tested the app on Firefox and Chromiumm for desktop, as well as DDG on mobile
    - Firefox displays certain grid sizes inproperly (last row consists of one elongated div)
- Launch
    - created a GitLab page for the app
- Review/ Community Review
    - shared for feedback in various communities
- Maintenance
    - considering rebuilding the grid function with CSS Grid
