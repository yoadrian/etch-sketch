//initial number of divs inside the drawing container
//number of divs (60) works together with css .grid-container size of 600px
//resulting in a flex-basis value of 10px (600 / 60) for the div inside the container

let click = true;

// Function that creates the initial Etch-a-Sketch board
let createInitialBoard = (divNumber) => {
    let gridContainer = document.querySelector('.grid-container');
    let pixels = gridContainer.querySelectorAll('div');

    pixels.forEach((div) => div.remove());

    for (i = 1; i <= (divNumber ** 2); i++) {
        let gridPixel = document.createElement('div');
        gridPixel.style.flexGrow = '1';
        gridPixel.style.flexShrink = '1';
        gridPixel.style.flexBasis = Math.floor(600 / divNumber) + 'px';
        gridContainer.appendChild(gridPixel);
    }
}

// Function attached to slider, that changes the amount of divs inside the container
function changeBoardSize() {
    let slider = document.getElementById('slider');
    let output = document.getElementById('output');
    output.innerHTML = slider.value; //this shows the initial number of divs

    slider.addEventListener('input', () => {
        turnOffGrid();
        output.innerHTML = slider.value;
        createInitialBoard(slider.value);
    });
}

// Function attached to the toggle switch, it toggles on/ off the grid lines
document.getElementById('switch').addEventListener('change', toggleGrid);

function toggleGrid() {
    let toggleSwitch = document.getElementById('switch');
    let gridContainer = document.querySelector('.grid-container');
    let pixels = gridContainer.querySelectorAll('div');
    
    if (toggleSwitch.checked) {
        pixels.forEach((div) => {
            div.style.border = '1px solid #d8d8d8';
        });
    } else {
        pixels.forEach((div) => {
            div.style.border = '';
        });
    }
}

// Function that switches the toggle grid button to off when the grid when it is being resized
let turnOffGrid = () => {
    let toggleSwitch = document.getElementById('switch');

    if (toggleSwitch.checked = true) {
        toggleSwitch.checked = false;
    }
}

// Function associated with the color picker, allows the user to select a color to draw
function pickColor() {
    let gridContainer = document.querySelector('.grid-container');
    let pixels = gridContainer.querySelectorAll('div');
    let gameFrame = document.querySelector('.game-frame');
    gameFrame.classList.remove('shake');
    
    pixels.forEach((div) => {
        div.removeEventListener('mouseover', randomizeOnMouseover);
        div.removeEventListener('mouseover', eraseOnMouseover);
        div.addEventListener('mouseover', drawOnMouseover);
    });
}

function drawOnMouseover() {
    if (click) {
        this.style.backgroundColor = document.getElementById('colorpick').value;
    }
}

document.getElementById('colorpick').addEventListener('click', pickColor);

// Function that draws with a random color and decreases in brightness on multiple passes, until div is black
function clickRandomize() {
    let gridContainer = document.querySelector('.grid-container');
    let pixels = gridContainer.querySelectorAll('div');
    let gameFrame = document.querySelector('.game-frame');
    gameFrame.classList.remove('shake');
    
    pixels.forEach((div) => {
        if (!div.style.backgroundColor) {
            div.style.backgroundColor = '#a6a6a6';
        }
        div.removeEventListener('mouseover', drawOnMouseover);
        div.removeEventListener('mouseover', eraseOnMouseover);
        div.addEventListener('mouseover', randomizeOnMouseover);
    });
}

function randomizeOnMouseover() {
    if (click) {
        let rgbString = this.style.backgroundColor;
        rgbArray = rgbString.substring(4, rgbString.length-1).replace(/ /g, '').split(',');
        let highest = Math.max(...rgbArray);
        let lowest = Math.min(...rgbArray);
        let lightness = (highest + lowest) / 2 / 255 * 100;
        if (lightness > 0) {
            this.style.backgroundColor = `hsl(${Math.random() * 360}, 100%, ${lightness - 7.2}%)`;
        }
    }
}

document.getElementById('random').addEventListener('click', clickRandomize);

//Function that allows the user to erase parts of the drawing
function clickErase() {
    let gridContainer = document.querySelector('.grid-container');
    let pixels = gridContainer.querySelectorAll('div');
    let gameFrame = document.querySelector('.game-frame');
    gameFrame.classList.remove('shake');

    pixels.forEach((div) => {
        div.removeEventListener('mouseover', drawOnMouseover);
        div.removeEventListener('mouseover', randomizeOnMouseover);
        div.addEventListener('mouseover', eraseOnMouseover);
    });
}

function eraseOnMouseover() {
    if (click) {
        this.style.backgroundColor = '';
        console.log(this.style.backgroundColor);
    }
}

document.getElementById('eraser').addEventListener('click', clickErase);

// Function that resets the board to only its background color
function resetBoard() {
    let gridContainer = document.querySelector('.grid-container');
    let pixels = gridContainer.querySelectorAll('div');
    let gameFrame = document.querySelector('.game-frame');
    gameFrame.classList.add('shake');
    pixels.forEach((div) => {
        div.removeEventListener('mouseover', drawOnMouseover);
        div.removeEventListener('mouseover', randomizeOnMouseover);
        div.style.backgroundColor = '';
    });
}

document.getElementById('reset').addEventListener('click', resetBoard);

//Function that toggles boolean value
//Allows to click/ unclick the painting brush
//Informs the user if brush is On or Off
document.querySelector('.grid-container').addEventListener('click', () => {
    click = !click;
    let colorMode = document.querySelector('.color-mode');
    if (click) {
        colorMode.innerText = 'On';
    } else {
        colorMode.innerText = 'Off'
    };
});

//Function to unhide and hide game information
document.querySelector('.info-container').addEventListener('click', () => {
    document.querySelector('.instructions').classList.toggle('hide');
    document.querySelector('.instruction').classList.toggle('unhide');
});

document.querySelector('.hide').addEventListener('click', () => {
    document.querySelector('.instructions').classList.toggle('unhide');
    document.querySelector('.instructions').classList.toggle('hide');
});

createInitialBoard(16);
changeBoardSize();